#include "cartograph.h"

// TODO : Properly implement this function
void Cartograph::loadMap(uint16_t mapID){
  //memset(&data[0], 0, 512);
  pf_lseek(MAP_INDEX);
  mapAdrs = read32();
  
  for(int i = 0; i < mapID; i++){
    pf_lseek(mapAdrs);
    mapAdrs = read32();
    Serial.println(mapAdrs);
  }
  
  mapAdrs += 4;
  pf_lseek(mapAdrs);
  
  map_width = read8();
  map_height = read8();
}

// Move the map inside the ram
void Cartograph::shiftMap(byte dir){
  switch (dir){
    case DIR_RIGHT :
      for (uint16_t i = 1; i < BUFFER_SIZE; i++)
      {
       if (i%BUFFER_SIZE == 0)
        continue;
       map_buffer[i-1] = map_buffer[i]; 
      }
      break;
    case DIR_LEFT :
      for (uint16_t i = BUFFER_SIZE-1; i > 0; i--){
      if (i%BUFFER_SIZE == 0)
        continue;
       map_buffer[i] = map_buffer[i-1];
      }
      break;
    case DIR_UP :
      for(uint16_t i=0; i < BUFFER_SIZE - BUFFER_WIDTH; i++)
        map_buffer[i] = map_buffer[i+BUFFER_WIDTH];
       break;
    case DIR_DOWN :
      for(uint16_t i = BUFFER_SIZE-1; i > BUFFER_WIDTH; i--)
        map_buffer[i] = map_buffer[i-BUFFER_WIDTH];
       break;
     }
}

void Cartograph::loadRowOfData(uint16_t x, uint16_t y,char pos){
  uint32_t theAdress = map_width*y+mapAdrs+2+x;
  if (theAdress % 66048 == 0) // Very strange bug that happends if you try to read data from that adress
  {
    theAdress -- ;
    pf_lseek(theAdress);
    pf_read(map_buffer+pos*BUFFER_WIDTH-1,BUFFER_WIDTH,&br);
    return;
  }
  else
  {
  //pf_lseek(map_width*((uint32_t)(y+pos))+mapAdrs+2+x);
  pf_lseek(theAdress);
    //Serial.println(map_width*((uint32_t)(y+pos))+mapAdrs+2+x);
  pf_read(map_buffer+pos*BUFFER_WIDTH,BUFFER_WIDTH,&br);
  return;
  }
}

void Cartograph::buffer_map(uint16_t x, uint16_t y,bool reset){
  static uint16_t pos = 0;
  if (reset)
    pos = 0;
  if(pos >= BUFFER_HEIGHT)
    return;
  loadRowOfData(x,y+pos,pos);
  pos ++;
}



void Cartograph::draw(uint8_t map){
  draw(map, cam_x, cam_y);
}

void Cartograph::draw(uint8_t map, uint16_t x_offset, int16_t y_offset){
  //Get map adress
  // TODO: Implement proper map selection
  
  updateAnim();
  
  static uint16_t x;
  static uint16_t y;
  uint16_t tx = x_offset >> 3;
  uint16_t ty = y_offset >> 3;
  if ((y != ty))
  {
      if ((ty) < y){
          shiftMap(DIR_DOWN);
          loadRowOfData(x,ty,0);
      }
      else{
      shiftMap(DIR_UP);
      loadRowOfData(x,ty+6,6);
      }
  }
  if ((x != tx)){
    if ((tx) < x)
      shiftMap(DIR_LEFT);
    else
      shiftMap(DIR_RIGHT);

    buffer_map(tx, ty,true);
  }
  if ((x == tx) &&  (y == ty))
  {
    buffer_map(x, y,false);
  }
  x = tx;
  y = ty;
  for(uint8_t j = 0; j <7; j++){
    for(uint8_t i = 0; i < 12; i++){
      //Serial.println(data[(j*11)+ i + 512 - 77]);
      ultraDraw2(8*map_buffer[(j*BUFFER_WIDTH)+ i+1] + sprite_buffer,i*8-x_offset%8, j*8-y_offset%8);
      //drawFromSD(map_data[i]*8+spriteAdrs, i*8, j*8);
    }
  }
}

void Cartograph::loadData(uint32_t adress,void* ptr){ // load 8 bytes from the sd card into the data buffer
 pf_lseek(adress);
  pf_read(ptr,8,&br);
}

// TODO : Properly implement this function
// Load the sprites data into the data buffer
void Cartograph::loadTileset(byte tileset){
  pf_lseek(SPRITE_INDEX);
  uint32_t sprite_index = read32()+tileset*(8*MAX_SPRITE+4*MAX_ANIM);
  for (byte i = 0; i < MAX_SPRITE; i++)
    loadData(sprite_index+i*8,sprite_buffer + i*8);
  loadAnimData(sprite_index);
  loadCollisionData(0);
}

#define ANIM_START_ADRS 32*8
void Cartograph::registerAnim(byte _length, byte frequence, byte tileId, byte AnimID){
  /*for(byte i = 0; i < 1; i++)
    loadData(adress+8*(i+1),sprite_buffer + 8*(31-i)-8*AnimID);
  loadData(adress,sprite_buffer + tileId*8);*/

  anim_buffer[4*AnimID] = _length;
  anim_buffer[4*AnimID +1] = tileId;
  anim_buffer[4*AnimID + 2] = frequence;
  anim_buffer[4*AnimID + 3] = 0; // Animation position

  
}


void Cartograph::loadAnimData(uint32_t sprite_index){
  pf_lseek(sprite_index + MAX_SPRITE*8);
  byte buf[4*MAX_ANIM];
  pf_read(buf, 4*MAX_ANIM, &br);
  for (byte i = 0; i<MAX_ANIM; i++)
    registerAnim(1,buf[i*4+2],buf[i*4],i);
}
void Cartograph::updateAnim(){
  
  for (byte j = 0; j <MAX_ANIM; j ++)
  {
    byte animLength = anim_buffer[j*4];
    byte animTileId = anim_buffer[1+j*4];
    byte animFrequence = anim_buffer[2+j*4];
    byte *animPos = anim_buffer +3+j*4;
  
  
    
    if(animLength == 0)
      continue;
    
    if(gb.frameCount%animFrequence == 0)
    {
      if (*animPos < animLength)
        *animPos ++;
      else
        *animPos = 0;
      for(byte i = 0; i < 8; i ++){
        byte tmp_buffer = sprite_buffer[animTileId*8 + i];
        sprite_buffer[animTileId*8 + i] = sprite_buffer[animTileId*8 + i + 8];
        sprite_buffer[animTileId*8 + i + 8] = tmp_buffer;
      }
    }
  }
}

void Cartograph::loadCollisionData(uint32_t sprite_index){
  //tileCollisionData[0] = 0xF2;
  //tileCollisionData[1] = 0xFD;
  tileCollisionData[0] = 0x0;
  tileCollisionData[1] = 0x0;
  tileCollisionData[3] = 0x0;
  tileCollisionData[4] = 0x0;
}

bool Cartograph::isCollision(int x, int y){
  byte id = getTileId(x,y);
  return readBitFrom8(tileCollisionData[id/8],id%8);
}

uint8_t Cartograph::getTileId(int x, int y){
  //(j*BUFFER_WIDTH)+ i+1
  return map_buffer[((y - cam_y + cam_y%8) / 8 ) * BUFFER_WIDTH + (x - cam_x + cam_x%8)/8 + 1];
}

byte* Cartograph::getSpriteBuffer(){
  return sprite_buffer;
}

