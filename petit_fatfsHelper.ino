
#include "petit_fatfsHelper.h"


bool initPetitFat(){
  PFFS.begin(10, rx, tx);
}

byte rx()
{
  // The SPI Data Register is a read/write register 
  // used for data transfer between the Register File and the SPI Shift Register. 
  // Writing to the register initiates data transmission. Reading the register causes the Shift Register receive buffer to be read.
  SPDR = 0xFF; // dummy byte to initiate reading
  // loop_until_bit_is_set is an AVR libc macro
  // SPSR is SPI status register, SPIF is SPI interrupt flag, that is set after transmission is complete
  loop_until_bit_is_set(SPSR, SPIF);
  return SPDR; //return the SPDR (= value back from slave)
}

void tx(byte d)
{
  SPDR = d;
  loop_until_bit_is_set(SPSR, SPIF);
}

uint16_t read16(){
  byte b[2];
  pf_read(b,2,&br);
  return b[0]<<8 | b[1];
}

uint32_t read32(){
  byte b[4];
  pf_read(b,4,&br);
  return ((uint32_t)b[0])<<24 | ((uint32_t)b[1])<<16 | ((uint32_t)b[2]) << 8 | ((uint32_t)b[3]);
}

uint8_t read8(){
  byte b[1];
  pf_read(b,1,&br);
  return b[0];
}

bool readBitFrom8(uint8_t source,uint8_t rbit){
  return (source & 1<<rbit);
}

