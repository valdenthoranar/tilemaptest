#include "character.h"

void Character::draw(){
  gb.display.setColor(WHITE);
  gb.display.fillRect(x - cg.cam_x,y - cg.cam_y,8,8);
  ultraDraw2(spritePtr,x- cg.cam_x,y- cg.cam_y);
}

void Character::move(char x, char y){
  if ((x>0 && (cg.isCollision(this->x + 8,this->y) || cg.isCollision(this->x + 8,this->y+7))) || (x<0 && (cg.isCollision(this->x - 1,this->y) || cg.isCollision(this->x-1,this->y+7)) ))
    x = 0;

  if ((y>0 &&( cg.isCollision(this->x,this->y+8) || cg.isCollision(this->x + 7,this->y+8)) ) || (y<0 && (cg.isCollision(this->x,this->y-1) || cg.isCollision(this->x + 7,this->y-1)) ))
    y = 0;
    
  this->x += x;
  this->y += y;
}

void Character::tp(int x, int y){
  this->x =x;
  this->y = y;
}

void Character::setSprite(byte *ptr){
  spritePtr = ptr;
}

