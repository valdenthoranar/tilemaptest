#include <SPI.h>
#include <petit_fatfs.h>
#include <Gamebuino.h>
#include "cartograph.h"
#include "character.h"

#define MAX_DEBUG 
FATFS fs;
FRESULT res;
WORD br;

Cartograph cg;
Gamebuino gb;
Character player;
void setup() {
  Serial.begin(9600);
  
  gb.begin();

  player.setSprite(cg.getSpriteBuffer() + 11*8);
  player.tp(15*8,15*8);
  initPetitFat();

  res = pf_open("MHQ0000.DAT");
  if (res) {Serial.print(F("Error !"));Serial.print(res);while (1);}

  cg.loadTileset(1);
  /*cg.registerAnim(0x10+4*8,1,10,11,0);
  cg.registerAnim(0x10+11*8,1,5,4,1);
  cg.registerAnim(0x10+4*8,1,10,2,2);
  cg.registerAnim(0x10+4*8,1,10,6,3);*/
  cg.loadMap(1);
  
}
void loop() {
  // Do whatever you want
   if(gb.update()){
      if(gb.buttons.repeat(BTN_RIGHT,1)){
        //Serial.println((int)cg.cam_x);
        player.move(1,0);
        if (cg.cam_x + 76 - player.x < 35)
          cg.cam_x ++;
      }
      if(gb.buttons.repeat(BTN_LEFT,1)&& player.x > 0){
        //Serial.println((int)_x);
        player.move(-1,0);
        if (player.x - cg.cam_x < 35 && cg.cam_x > 0)
          cg.cam_x --;
      }
      if(gb.buttons.repeat(BTN_DOWN,1)){
        player.move(0,1);
        if (cg.cam_y + 40 - player.y < 17)
          cg.cam_y ++;
      }
      if(gb.buttons.repeat(BTN_UP,1) && player.y>0){
        player.move(0,-1);
        if (player.y - cg.cam_y < 17 && cg.cam_y > 0)
          cg.cam_y --;
      }
      
      //drawFromSD(0x18,gb.frameCount%92-8,gb.frameCount%56-8);
      cg.draw(0);
      player.draw();

      #ifdef MAX_DEBUG
      static long  max_frame = gb.frameDurationMicros;
      static long average_frame = gb.frameDurationMicros;
      if(gb.buttons.pressed(BTN_B)){
        max_frame = 0;
      }
      max_frame = max(max_frame,gb.frameDurationMicros);
      average_frame = (average_frame + gb.frameDurationMicros)/2;
      gb.display.setColor(BLACK,WHITE);
      gb.display.print(F("F:"));
      gb.display.print(gb.frameDurationMicros);
      gb.display.cursorX  = 0;
      gb.display.cursorY = 6;
      gb.display.print(F("T:"));
      gb.display.print(cg.getTileId(player.x,player.y));
      gb.display.print(F(" C:"));
      gb.display.print(cg.isCollision(player.x,player.y));
      #endif //MAX_DEBUG
    }
}


