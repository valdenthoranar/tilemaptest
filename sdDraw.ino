#include "sdDraw.h"

#define BUFFER_WIDTH 14
#define BUFFER_HEIGHT 7
#define BUFFER_SIZE BUFFER_WIDTH*BUFFER_HEIGHT

//Temporary fix for clipped draw when y > 40
void ultraDraw2(byte* data, char x, char y){
        
        if( y>47) //return if the sprite is not visible
          return;
        /*if(y >= 40){ //if the sprite is half off the screen at the bottom, clean the sprite
          uint8_t altBuf[8]; // create a buffer for the masked sprite
          byte mask = ((1<<(48-y))-1); //create the mask for the sprite
          for (uint8_t i = 0; i < 8 ; i++)
            altBuf[i] = data[i] & mask; //mask the sprite
          ultraDraw4(altBuf,x,y); // and draw the masked sprite
          return;
        }*/
        // if all is ok, call the original function
        ultraDraw4(data,x,y);
}

//Created by Sorunome
//Draw 8x8 sprites on the screen, realy fast
//data store the sprite (8 bytes)
//x the x position on the screen 
//y the y position on the screen
//(0,0)------->(84,0)
//  |
//  |
//  V
//(0,48)
void ultraDraw4(byte* data, char x, char y){
        uint8_t* buf = (((y+8)&0xF8)>>1) * 21 + x + gb.display.getBuffer();
        asm volatile(
        "mov R20,%[y]\n\t"
        "ldi R17,7\n\t"
        "add R20,R17\n\t"
        "brmi End\n\t"
        "cpi %[y],48\n\t"
        "brpl End\n\t"
       
        "inc R20\n\t"
        "ldi R16,8\n\t"
        "andi R20,7\n\t"
        "cpi R20,0\n\t"
        "breq LoopAligned\n"
        "LoopStart:\n\t"
         
          "tst %[x]\n\t"
          "brmi LoopSkip\n\t"
          "cpi %[x],84\n\t"
          "brcc LoopSkip\n\t"
         
          "ld R17,Z\n\t"
          "eor R18,R18\n\t"
          "mov R19,R20\n\t"
          "clc\n\t"
          "LoopShift:\n\t" // carry is still reset from the cpi instruction or from the dec
            "rol R17\n\t"
            "rol R18\n\t"
            "dec R19\n\t"
            "brne LoopShift\n\t"
 
          "tst %[y]\n\t"
          "brmi LoopSkipPart\n\t"
         
          "ld R19,X\n\t"
          "or R19,R17\n\t"
          "st X,R19\n\t"
        "LoopSkipPart:\n\t"
 
          "cpi %[y],41\n\t"
          "brpl LoopSkip\n\t"
         
          "ld R19,Y\n\t"
          "or R19,R18\n\t"
          "st Y,R19\n\t"
 
        "LoopSkip:\n\t"
          "eor R18,R18\n\t"
          "ldi R19,1\n\t"
          "add R26,R19\n\t" // INC DOESN'T CHANGE CARRY!
          "adc R27,R18\n\t"
          "add R28,R19\n\t"
          "adc R29,R18\n\t"
          "add R30,R19\n\t"
          "adc R31,R18\n\t"
         
          "inc %[x]\n\t"
          "dec R16\n\t"
          "brne LoopStart\n\t"
        "rjmp End\n"
        "LoopAligned:\n\t"
          "tst %[x]\n\t"
          "brmi LoopAlignSkip\n\t"
       
          "cpi %[x],84\n\t"
          "brcc LoopAlignSkip\n\t"
         
          "ld R17,Z\n\t"
          "ld R18,X\n\t"
          "eor R18,R17\n\t"
          "st X,R18\n\t"
        "LoopAlignSkip:\n\t"
          "ldi R18,1\n\t"
          "add R26,R18\n\t"
          "adc R27,R20\n\t"
          "add R30,R18\n\t"
          "adc R31,R20\n\t"
          "inc %[x]\n\t"
          "dec R16\n\t"
          "brne LoopAligned\n"
        "End:\n\t"
        ::"x" (buf - 84),"y" (buf),"z" (data),[y] "r" (y),[x] "r" (x):"r16","r17","r18","r19","r20");
 
}

