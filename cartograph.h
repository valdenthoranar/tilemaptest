#ifndef _CARTOGRAPH_H_
#define _CARTOGRAPH_H_

#include <petit_fatfs.h>
#include <Gamebuino.h>
#include "sdDraw.h"
// definitions of the position of the map buffer inside the RAM
#define BUFFER_WIDTH 14 //Size of the MAP buffer on the X axis
#define BUFFER_HEIGHT 7 //Size of the Map buffer along the y axis
#define BUFFER_SIZE BUFFER_WIDTH*BUFFER_HEIGHT
#define START_MAP_DATA 512-BUFFER_SIZE
#define MAX_ANIM 4 // the max number of anims
#define W_POS 511 - BUFFER_SIZE - 2 //the position in the ram of the Width of the map
#define H_POS W_POS+1               //The position in the ram of the Height of the map

//SPRITE DATA
#define MAX_SPRITE 32


//SD CARD INDEXES LOCATION
#define SPRITE_INDEX 0x03
#define MAP_INDEX 0x07

//Enumeration for the directions of the shifts
enum shiftDirection{
  DIR_LEFT,
  DIR_RIGHT,
  DIR_UP,
  DIR_DOWN
} ;

class Cartograph {
  public :
  /** Register animation
   * @param _length the length of the animation in sprites. (WARNING, AT THE MOMENT IT DOES NOTHING. WILL CERTAINLY BE CAPPED AT 2 FRAMES PER ANIM )
   * @param frequence the nubers of frame to wait before going to the next frame 
   * @param tileId the id of the tile that will be used for the animation
   * @param AnimId the id of the animation (should be between 0 and MAX_ANIM-1)
   */
    void registerAnim( byte _length, byte frequence, byte tileId, byte AnimID);
    
  /** Load a map. At the moment, it only serves as an initialisation of the lib
   * 
   * @param mapID the id of the map to load. Does nothing at the moment
   */
    void loadMap(uint16_t mapID);

    /** Draw the map on the screen. Will aslo refreches the animations and take care of scrolling
     * 
     */
    void draw(uint8_t map);
    void draw(uint8_t map, uint16_t x_offset, int16_t y_offset);

    /** Load a tilset. It's not fully implemented yet and serves as an initialisation at the moment
     * 
     */
    void loadTileset(byte tileset);

    /** Set a specific sprite in the RAM. Not implemented at the moment
     * 
     */
    void setSprite(byte spriteId, uint32_t spriteAdress);


    void loadAnimData(uint32_t sprite_index);

    void loadCollisionData(uint32_t sprite_index);
    byte* getSpriteBuffer();

    int cam_x = (15-4)*8;
    int cam_y = (15-4)*8;

  bool isCollision(int x, int y);
  
  uint8_t getTileId(int x, int y);

    
  private :
    uint32_t mapAdrs = 0;
    byte sprite_buffer[MAX_SPRITE*8];
    byte tileCollisionData[(MAX_SPRITE%8 == 0 ? MAX_SPRITE/8:MAX_SPRITE/8 + 1)];
    byte map_buffer[BUFFER_SIZE];
    byte map_width;
    byte map_height;
    byte anim_buffer[MAX_ANIM*4];
    
    void loadData(uint32_t adress,void* ptr);
    void shiftMap(byte dir);
    void buffer_map(uint16_t x, uint16_t y,bool reset);
    void loadRowOfData(uint16_t x, uint16_t y,char pos);
    void updateAnim();
};

#endif //_CARTOGRAPH_H_
