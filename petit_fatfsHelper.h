#ifndef _PETIT_FAT_FS_HELPER_H_
#define _PETIT_FAT_FS_HELPER_H_
#include <petit_fatfs.h>

bool initPetitFat();

byte rx();

void tx(byte d);
#endif
