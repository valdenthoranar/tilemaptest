#ifndef _CHARACTER_H_
#define _CHARACTER_H_
class Character{
  public :
  void setSprite(byte *ptr);
  void draw();
  void move(char x, char y);
  void tp(int x, int y);
  
  int x;
  int y;
  
  private :

  
  byte *spritePtr;
};


#endif //_CHARACTER_H_
